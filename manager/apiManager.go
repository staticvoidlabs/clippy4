package manager

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
)

// InitAPIService starts the API service.
func InitAPIService() {

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", getStateInfoService)
	router.HandleFunc("/clippy/queue", getQueueStatus)
	router.HandleFunc("/clippy/queue/add/{vid}", addDownloadJob)
	//router.PathPrefix("/nappy/news").Handler(http.StripPrefix("/nappy/news", http.FileServer(http.Dir("./www"))))

	// Start rest service.
	//go log.Fatal(http.ListenAndServe(":9100", router))
	go http.ListenAndServe(":9100", router)
}

// Endpoint implementation.
func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	json.NewEncoder(w).Encode("getStateInfoService")
}

func addDownloadJob(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	mVideoID := vars["vid"]

	BuildDownloadJob(mVideoID, mCurrentConfig)

	if mCurrentConfig.DebugLevel == 1 {
		fmt.Println("Added video ID to queue: ", mVideoID)
	}

	json.NewEncoder(w).Encode("addDownloadJob > VideoID: " + mVideoID)
}

func getQueueStatus(w http.ResponseWriter, r *http.Request) {

	/*
		// TESTING START
		mDownloadQueue[0].State = "DOWNLOADING"
		mDownloadQueue[0].VideoID = "12345678"
		mDownloadQueue[0].FileSize = "5 MB"
		mDownloadQueue[0].VideoTitle = "Test-Video Nr. 1"

		mDownloadQueue[2].State = "FAILED"
		mDownloadQueue[2].VideoID = "12345678"
		mDownloadQueue[2].FileSize = "1 MB"
		mDownloadQueue[2].VideoTitle = "Test-Video Nr. 2"

		mDownloadQueue[5].State = "FINISHED"
		mDownloadQueue[5].VideoID = "12345678"
		mDownloadQueue[5].FileSize = "12 MB"
		mDownloadQueue[5].VideoTitle = "Test-Video Nr. 3"

		mDownloadQueue[6].State = "other"
		mDownloadQueue[6].VideoID = "12345678"
		mDownloadQueue[6].FileSize = "12 MB"
		mDownloadQueue[6].VideoTitle = "Test-Video Nr. 3"
		// TESTING END
	*/

	// Marshall queue as JSON.
	tmpQueueStatusAsJSON, err := json.Marshal(mDownloadQueue)

	if err != nil {
		fmt.Println(err)
		return
	}

	w.Write(tmpQueueStatusAsJSON)
}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	// Try to parse template.
	tmpl, err := template.ParseFiles("./www/template_news_simple.html")
	if err != nil {
		fmt.Println("Error parsing template: ", err)
	}

	err = tmpl.Execute(w, mDownloadQueue)
	if err != nil {
		fmt.Println("Error executing template: ", err)
	}

	//json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:success.")
}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}
