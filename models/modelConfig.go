package models

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version                    string   `json:"version"`
	DebugLevel                 int      `json:"debugLevel"`
	RunAsRemoteTargetOnly      bool     `json:"runAsRemoteTargetOnly"`
	PathDownloads              string   `json:"pathDownloads"`
	YtDlLoggingEnabled         bool     `json:"ytDlLoggingEnabled"`
	FullPathYtDl               string   `json:"fullPathYtDl"`
	ArgList                    []string `json:"argList"`
	ThumbailCacheDir           string   `json:"thumbailCacheDir"`
	YoutubeAPIKey              string   `json:"ytApiKey"`
	YoutubeBaseURL             string   `json:"ytBaseURL"`
	YoutvPostProcessingEnabled bool     `json:"youtvPostProcessingEnabled"`
	RemoteClippyBaseURL        string   `json:"remoteClippyBaseURL"`
}
